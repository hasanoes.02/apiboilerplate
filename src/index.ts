import * as dotenv from "dotenv"
import express, {Express} from "express"
import {Logger} from "./config/Logger"
import {defaultRouter} from "./api/v1/routes"
import morgan from "morgan"
import swaggerUi from "swagger-ui-express"
import "reflect-metadata"
import {databaseSource} from "./config/DatabaseConfig";
import {errorHandler} from "./api/v1/middlewares/error.middleware";

dotenv.config()

const app: Express = express()
const log: Logger = new Logger("CELER MSHOP")

app.use(express.json())
app.use(morgan("tiny"))
app.use(express.static("public"))

app.use(errorHandler)
// app.use(notFoundHandler)

app.use("/docs", swaggerUi.serve, swaggerUi.setup(undefined, {
    swaggerOptions: {
        url: "/swagger.json"
    }
}))

app.use("/api/v1", defaultRouter)

databaseSource
    .initialize()
    .then(() => {
        console.log("Data Source has been initialized!")
    })
    .catch((err) => {
        console.error("Error during Data Source initialization:", err)
    })

const PORT: number = parseInt(process.env.PORT as string, 10)

app.listen(PORT, () => {
    log.info(`Now Listening on port ${PORT}.`)
})
