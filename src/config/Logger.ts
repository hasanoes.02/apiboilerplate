export class Logger {

    constructor(private prefix: string) {
    }

    debug(message: string, ...data: any[]) {
        return console.debug(`[${this.prefix} - DEBUG]: ${message}`, data)
    }

    info(message: string) {
        return console.info(`[${this.prefix} - INFO]: ${message}`)
    }
}
