import {DataSource} from "typeorm";
import {UserModel} from "../api/v1/models/user.model";

export const databaseSource = new DataSource({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "passsword",
    database: "mshop",
    logging: true,
    synchronize: true,
    entities: [UserModel],
})
