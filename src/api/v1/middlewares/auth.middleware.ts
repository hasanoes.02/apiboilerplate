import {NextFunction, Request, Response} from "express";
import jwt from "jsonwebtoken";

export const authMiddleware = {

    checkJwt(req: Request, res: Response, next: NextFunction) {
        const token = <string>req.headers["auth"]
        let jwtPayload

        try {
            jwtPayload = <any>jwt.verify(token, process.env.JWT_SECRET!)
            res.locals.jwtPayload = jwtPayload
        } catch (e) {
            res.status(401).send()
            return
        }

        const {id, username} = jwtPayload
        const newToken = jwt.sign({ id, username }, process.env.JWT_SECRET!, {
            expiresIn: process.env.JWT_EXPIRATION
        })
        res.setHeader("token", newToken)

        next()
    }
}