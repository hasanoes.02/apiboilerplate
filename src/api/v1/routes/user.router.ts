import {Request, Response, Router} from 'express'
import UserController from "../controllers/user/user.controller";

export const userRoute = Router()

const controller = new UserController()

userRoute.get('/', async (req: Request, res: Response) => {
    const response = await controller.getUsers()

    return res.send(response)
})

userRoute.get('/:id([0-9]+)', async (req: Request, res: Response) => {
    const response = await controller.getUserById(Number(req.params.id))

    return res.send(response)
})

userRoute.post('/', async (req: Request, res: Response) => {
    const response = await controller.createUser(req.body)

    return res.send(response)
})
