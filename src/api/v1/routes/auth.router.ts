import {Request, Response, Router} from 'express'
import AuthController from "../controllers/auth/auth.controller";

export const authRoute = Router()

const controller = new AuthController()

authRoute.post("/login", async (req: Request, res: Response) => {
    const response = controller.loginUser()

    return res.send(response)
})

authRoute.post("/register", async (req: Request, res: Response) => {
    const response = controller.registerUser()

    return res.send(response)
})
