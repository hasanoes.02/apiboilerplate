import {Router} from 'express'
import {userRoute} from "./user.router"
import {authRoute} from "./auth.router";

export const defaultRouter = Router()

defaultRouter.use('/user', userRoute)
defaultRouter.use('/auth', authRoute)
