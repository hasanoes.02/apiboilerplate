import {UserRole} from "./UserRole";

export type JwtPayload = {
    id: number
    name: string
    role: UserRole
}