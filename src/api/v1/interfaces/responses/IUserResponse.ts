export interface IGetUserResponse {
    /**
     * The unique user ID.
     */
    id: number
    username: string
    /**
     * The email the user used to register account.
     */
    email: string
}