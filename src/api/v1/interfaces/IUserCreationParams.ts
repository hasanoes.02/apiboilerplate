export interface IUserCreationParams {
    username: string
    email: string
}