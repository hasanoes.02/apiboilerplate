import mockupData from "../../../mockup/users.json"
import {IUserCreationParams} from "../interfaces/IUserCreationParams";
import UserRepository from "../repositories/user.repository";

const userRepository = new UserRepository()

export class UserService {
    getAllUsers() {
        return mockupData
    }

    getOneUser(id: number) {
        return userRepository.getUserById(id)
    }

    createUser(params: IUserCreationParams) {
        try {
            userRepository.createUser(params).then(() => {
                return "Created"
            })
        } catch (e) {
            return e
        }
    }
}
