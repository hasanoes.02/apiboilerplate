import {UserModel} from "../models/user.model";
import {databaseSource} from "../../../config/DatabaseConfig";
import {IUserCreationParams} from "../interfaces/IUserCreationParams";

export default class UserRepository {

    async createUser(data: IUserCreationParams) {
        const body = new UserModel()
        body.email = data.email
        body.username = data.username

        const user = await databaseSource.getRepository(UserModel).create(body)

        return databaseSource.getRepository(UserModel).save(user)
    }

    async getUserById(id: number) {
        const result = await databaseSource.getRepository(UserModel).findOneBy({
            id
        })

        return result
    }
}
