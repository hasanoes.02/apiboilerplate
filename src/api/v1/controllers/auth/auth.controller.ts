import {Post, Route, Tags} from "tsoa";

@Route("api/v1/auth")
@Tags("Auth")
export default class AuthController {

    @Post("/register")
    public async registerUser() {

    }

    @Post("/login")
    public async loginUser() {

    }
}