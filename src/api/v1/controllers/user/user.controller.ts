import {IGetUserResponse} from "../../interfaces/responses/IUserResponse"
import {Body, Example, Get, Path, Post, Put, Route, SuccessResponse, Tags} from "tsoa"
import {UserService} from "../../services/user.service";
import {IUserCreationParams} from "../../interfaces/IUserCreationParams";

const service = new UserService()

@Route("api/v1/user")
@Tags("User")
export default class UserController {

    @Get("/")
    public async getUsers(): Promise<IGetUserResponse[]> {
        return service.getAllUsers()
    }

    /**
     * Retrieves the details of an existing user.
     * Supply the unique user ID from either and receive corresponding user details.
     * @param userId The user's identifier
     * @pattern [1-9][0-9]*
     * @example userId 1
     * @example userId 125
     */
    @Example<IGetUserResponse>({
        id: 352,
        username: "Weasel",
        email: "hello@mail.com"
    })
    @Get("{userId}")
    public async getUserById(@Path() userId: number)/*: Promise<IGetUserResponse>*/ {
        return service.getOneUser(userId) // as IGetUserResponse
    }

    @Post("/")
    @SuccessResponse("201", "Created")
    public async createUser(@Body() requestBody: IUserCreationParams) {
        return service.createUser(requestBody)
    }

    @Put("/")
    public async updateUser() {
        return service.getAllUsers()
    }
}
