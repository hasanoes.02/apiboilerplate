import UserController from "./user.controller";

describe("UserController", () => {
    describe("getUser", () => {
        test("should return empty object", async () => {
            const controller = new UserController()
            const response = await controller.getUsers()

            expect(response).toBe("Test User Controller")
        })
    })
})
